// Directions:
// Create two event listeners to an event when a user types in the first and last name inputs
// When this event triggers, update the span-full-name's content to show the value of the first name input on the left and the value of the last name input on the right

// Stretch goal: Instead of an anonymous function, create a new function that the two event listers will call

// Guide question: Where do the names come from and where should they go ?


const inputFirstName = document.querySelector('#txt-first-name');
const inputLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


const displayFullName = () => {
	spanFullName.innerHTML = `${inputFirstName.value} ${inputLastName.value}`
}


inputFirstName.addEventListener('keyup', displayFullName);
inputLastName.addEventListener('keyup', displayFullName);


